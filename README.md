# Frontend Assignment

Description
There are two parts for the assignment:
1. Converter Form
2. Wallet Details Popup

Converter Form
A form with two inputs named NEP and BUSD respectively. If any value is entered in
one of the inputs, the other input’s value gets updated with the corresponding
converted amount. The functionality is almost the same as a currency converter in
Google. For the sake of simplicity, assume 1 NEP is equal to 3 BUSD.

Wallet Details Popup
A popup shows whether the wallet is connected or not. We use metamask as the
wallet. If the wallet is connected, details of the wallet (connected Chain ID and
balance) should be displayed on a table. Also, a button to disconnect the wallet should
also be provided on the screen some

## TODO
First you have all the necessary tools to run the project.

Check if you have installed the following:

for this project we need
NodeJS | npm | ReactJS | NextJS | Web3React | MetaMask Wallet

Clone the project from | git clone git@bitbucket.org:rob_panes/frontend-assignment.git

-> run after the clone | npm install
-> after all the packages was installed run | npm run dev

## Available Scripts

In the project directory, you can run:

### `npm run dev`
To run the porject in local.
The provided host is http://localhost:3000

### `npm run build`
To build the project.

### `npm run start`
To run the project in production.
