
import { useState, useEffect } from 'react';
import { useWeb3React } from "@web3-react/core";
import { injected } from "../wallet/connector";
import { formatEther } from '@ethersproject/units';
import { Web3Provider } from '@ethersproject/providers';
import { Spinner } from '../utils/spinner';
import Cookies from 'universal-cookie';

interface cbProps {
	clickFunction: (event: React.MouseEvent<HTMLButtonElement>) => void
 }

const Modal = (Props: cbProps) => {

	const [balance, setBalance] = useState<null | undefined>();
	const [loading, setLoading] = useState<boolean>(true)

	const { active, account, library, chainId, connector, activate, deactivate } = useWeb3React<Web3Provider>()

	const cookie = new Cookies();

	useEffect((): any => {
		
		if ( !!account && !!library ) {

		  let stale = false;

		  cookie.set('account', account);

		  library
			.getBalance(account)
			.then((balance: any) => {
				if (!stale) {
					setBalance(balance)
					setLoading(false)
				}
			})
			.catch(() => {
				if (!stale) {
					setBalance(null)
					setLoading(false)
				}
			})
	
		  return () => {
				stale = true
				setBalance(undefined)
				setLoading(false)
		  }
		}
	}, [account, library, chainId])

	const connect = async () => {
		try {
			await activate(injected)
		} catch (ex) {
			console.log(ex)
		}
	}
  
	const disconnect = async () =>  {
		try {
			cookie.remove('account');
			deactivate()
		} catch (ex) {
			console.log(ex)
		}
	}

	return (
		<div className='appmodal-container'>
			<div className="appmodal">
				<img src='../../neptune-mutual.svg' />
				<div className="content">
					<div>
						{
							active ? 
								<>
									<div className='align-center bg bg-info'>Connected</div> 
									<table className='account-table'>
										<thead>
											<tr>
												<th></th>
												<th>Value</th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td>Account</td>
												<td><span>{account === null? '-': account? `${account.substring(0, 6)}...${account.substring(account.length - 4)}`: ''}</span></td>
											</tr>
											<tr>
												<td>Chain ID</td>
												<td><span>{chainId ?? ''}</span></td>
											</tr>
											<tr>
												<td>Balance</td>
												<td>{loading && <Spinner color={'white'} style={{ height: '10%', marginRight: '0' }} />}<span>{balance === null ? 'Error' : balance ? `Ξ${formatEther(balance)}` : ''}</span></td>
											</tr>
										</tbody>
									</table>
								</>
							: 
								<div className='align-center bg bg-warning'>Not connected</div>
						}

					</div>
					{!active && <button onClick={connect}>Connect to Metamask</button>}
					{active && <button onClick={disconnect} className="bg bg-warning">Disconnect</button>}
					<button onClick={Props.clickFunction} style={{marginTop:`5px`}}>Cancel</button>
				</div>
			</div>
		</div>
	);

}

export default Modal;