import { useState, useEffect } from 'react';
import Modal from './modal'; 

const App = () => {

	const [nepValue, setNepValue]   = useState<string | number | any>();
	const [busdValue, setBusdValue] = useState<string | number | any>();
	const [isClose, setModalClose] = useState<boolean>(true);

	const getDecimalNoRoundOff = (n:number) => {

		if ( isNaN(n) ) {
			return '';
		}

		const _n = n.toFixed(3);
		const _value = _n.substring(0, _n.length - 1);
		return _value;
	}

	const handleFocus = (event: React.ChangeEvent<HTMLInputElement>) => event.target.select();

	const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {

		const _value  = event.currentTarget.value;
		const pattern = /^(\s*|\d+)(\.*)(\s*|\d+)$/

		if ( ! pattern.test(_value) ) {
			return '';
		}
		
		switch( event.target.name ) {
			case 'nep':
				var _conversion = getDecimalNoRoundOff(parseFloat(_value) * 3)
				setNepValue(_value);
				setBusdValue(_conversion);
				break;
			case 'busd':
				var _conversion = getDecimalNoRoundOff(parseFloat(_value) / 3)
				setNepValue(_conversion);
				setBusdValue(_value);
				break;
		}
	}

	const onCloseModal = () => {
		setModalClose(true);
	}

	const openModal = () => {
		setModalClose(false);
	}

	return (
		<div className="App">
			{!isClose && <Modal clickFunction={onCloseModal}/>}
			<div className='app-wrapper centered'>
				<header>
					<img src='../neptune-mutual.svg' />
					<h1>Crypto Conventer</h1>
				</header>
				<div className='form'>
					<div className='block'>
						<label>NEP</label>
						<input type="text" name="nep" value={nepValue} onChange={handleChange} onFocus={handleFocus} />
					</div>
					<div className='block'>
						<label>BUSD</label>
						<input type="text" name="busd" value={busdValue} onChange={handleChange} onFocus={handleFocus} />
					</div>
					<div className='block'>
						<button onClick={openModal}>Check Metamask Wallet</button>
					</div>
				</div>
			</div>
		</div>
	);
}

export default App;
