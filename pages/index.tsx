import React from 'react';

import App from '../components/app/App';

const Index = () => {
  return(
    <React.StrictMode>
      <App />
    </React.StrictMode>
  );
}

export default Index;