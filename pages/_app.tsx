import { Web3ReactProvider } from '@web3-react/core';
import { Web3Provider } from '@ethersproject/providers';
import Web3 from 'web3'

import App from 'next/app';
import Head from 'next/head';

import './style.css';

const getLibrary = (provider: any): Web3Provider => {
    const library = new Web3Provider(provider)
    library.pollingInterval = 12000
    return library
  }

class FrontendAssignment extends App {

    render() {

        const { Component, pageProps } = this.props;

        return (
            <>
                <Head>
                    <meta name='viewport' content='width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=5' />
                </Head>
                <Web3ReactProvider getLibrary={getLibrary}>
                    <Component {...pageProps}/>
                </Web3ReactProvider>
            </>

        )
    }
}

export default FrontendAssignment